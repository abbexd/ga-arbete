function Bullet(){
    this.x;
    this.y = 401;
    let direction = [5, -5];
    this.speed = direction;
    this.maxTick = 25;
    this.tick = this.maxTick;

    this.Draw = function(){
        //creates a new instance of the bullet sprite everytime spacebar is pressed, the player shoots.
        if(keyWentDown(controls[0]) && this.tick === this.maxTick){
        this.shoot = createSprite(player.player.position.x, this.y, this.x, this.y);
        this.shoot.addImage(bulletImage);
        weaponSound.play();
        this.shoot.immovable = true;

        //adds speed to the bullet from player direction
        this.shoot.velocity.x = this.speed[playerdir];
        this.tick = 0;
        } else if(this.tick < this.maxTick){
            this.tick++;
        }
    }

    //should work but doesn't
    this.Collision = function(){
        if(keyWentDown(controls[0])){
        this.shoot.setDefaultCollider();
        this.shoot.bounce(enemy.enemy);
        }
        drawSprites();
    }
}