function Enemy(){
    this.x = random(enemyPosition);
    this.y = 403;
    this.health = random(enemyHealth);
    this.speed = random(0.001, 0.03);
    this.enemy = createSprite(this.x, this.y, this.x, this.y);
    this.enemy.addImage("still", zombieStill);
    this.enemy.addAnimation("walk", "assets/sprites/zombie1.png", "assets/sprites/zombie2.png");


    this.Draw = function(){
      if(this.x < 11){
        this.enemy.mirrorX(-1);
      }
      this.enemy.changeAnimation("walk");
      //pulls the enemy towards the center of the canvas
      this.enemy.attractionPoint(this.speed, 300, this.y);
      this.enemy.setDefaultCollider();
    }
}