function preload(){
    backgroundImage = loadImage("assets/background.png");

    playerStill = loadImage("assets/sprites/player_weapon0.png");

    zombieStill = loadImage("assets/sprites/zombie0.png");

    bulletImage = loadImage("assets/sprites/bullet.png");

    backgroundMusic = loadSound("assets/backgroundmusic.mp3");
    weaponSound = loadSound("assets/weaponsound.mp3");
}

function setup(){
    createCanvas(600, 600);
    frameRate(60);
    noCursor();
    backgroundMusic.play();
    backgroundMusic.setVolume(0.3);

    bullet = new Bullet();
    
    player = new Player();

    enemy = new Enemy();
}

function draw(){
    
    //background
    background(0);
    noStroke();
    image(backgroundImage, 0, 0);

    //bullet
    bullet.Draw();
    bullet.Collision();

    //player
    player.Draw();

    //enemy
    enemy.Draw();

    /*
    //enemy spawns
    let enemyMultiplier = 1;
    let spawnInterval = 3000;
    setInterval(enemySpawn, spawnInterval*enemyMultiplier);
    function enemySpawn(){
        let spawner = int(random(2));
        switch(spawner){
            case 0:
                enemy.push(new Enemy(10, enemy.enemy.y));
                break;
            case 1: 
                enemy.push(new Enemy(590, enemy.enemy.y));
                break;
        }
    }*/
} 