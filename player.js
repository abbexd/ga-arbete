function Player(){
  this.x = width/2;
  this.y = 400;
  this.health = 100;
  this.speed = 2;
  this.player = createSprite(this.x, this.y, this.x, this.y);
  this.player.addImage("still", playerStill);
  this.player.addAnimation("walk", "assets/sprites/player_weapon1.png", "assets/sprites/player_weapon2.png");

   this.Draw = function(){
     this.player.changeImage("still");
      if(keyIsDown(controls[1])){
        this.x -= this.speed;
        playerdir = 1;
        this.player.mirrorX(-1);
        this.player.changeAnimation("walk");
        this.player.setCollider("rectangle", 6, 0, 18, 47);
        if(this.x < 15){
          this.x = 15;
        } else{
          this.player.setCollider("rectangle", 6, 0, 18, 47);
        }
      }
      if(keyIsDown(controls[2])){
        this.x += this.speed;
        playerdir = 0;
        this.player.mirrorX(1);
        this.player.changeAnimation("walk");
        this.player.setCollider("rectangle", -6, 0, 18, 47);
        if(this.x > 585){
          this.x = 585;
        } else{
          this.player.setCollider("rectangle", -6, 0, 18, 47);
        }
      }
      this.player.immovable = true;
      this.player.position = createVector(this.x, this.y);
      this.player.debug = true;
      this.player.bounce(enemy.enemy);
    }
}

